package com.localbizonline.localpros.utils;

public interface Prefs {

    String PREF_FILE = "LocalPros";
    String USER_LOGIN = "login";
    String ALERT = "alert";
    String AUTH_KEY = "numbersss";
    String id = "id";
    String parent_id = "parent_id";
    String company_id = "company_id";
    String first_name = "first_name";
    String last_name = "last_name";
    String password = "password";
    String company_name = "company_name";
    String profile_picture = "profile_picture";
    String state = "state";
    String email = "email";
    String email_verified_at = "email_verified_at";
    String phone_number = "phone_number";
    String address = "address";
    String country = "country";
    String city = "city";
    String latitude = "latitude";
    String longitude = "longitude";
    String status = "status";
    String userstatus = "user_status";
    String role = "role";
    String token = "token";
    String deviceToken = "device_token";
    String devicetype = "device_type";
    String userUnique = "user_unique_code";
    String unauth = "unauth";
    String name = "name";
    String company_image = "company_image";
    String updated_at = "updated_at";
    String created_at = "created_at";
    String deleted_at = "deleted_at";
    String commission = "commission";
    String commission_type = "commission_type";
    String user_unique_code = "user_unique_code";
    String authy_id = "authy_id";
    String documentstatus = "document_status";
    String vehicle = "vehicle";
    String contact_person = "contact_person";
    String taxId = "tax_id";
    String support_number = "support_number";
    String Forcetoken = "force_token";
    String date = "date";

}
